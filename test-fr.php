<?php
error_reporting(-1);
ini_set('display_errors', 'On');

$config = array('database' => './db.sqlite',
                'table' => 'table',
                'table_max_length' => 1024,
                'key_min_length' => 24,
                'key_max_length' => 1024,
                'p_max_length' => 10,
                'd_max_length' => 1048576,
                'set_public_data' => true);

require 'backend.sqlite.php';
require 'messages-fr.php';

processRequest();
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
  <title>Test de formin</title>
  <script src="formin.js" type="text/javascript"></script>
  <script src="marked.min.js" type="text/javascript"></script>
  <script src="markedslow.js" type="text/javascript"></script>
  <link href="formin.css" rel="stylesheet" type="text/css">
</head>
<body>
<header>
<h1 class="title">Test de Formin</h1>
</header>

<noscript>
<div class="formin-notification formin-notification-critical">
Attention : la majeure partie des fonctionnalités de ce formulaire nécessite l’activation de JavaScript.
</div>
</noscript>
<?php showNotifications(); ?>

<form id="formulaire" method="post" onsubmit="callbacks.onSubmit();">

<ul>
<li>
<label>
  Quelles sont vos initiales ?
  <input type="text" name="d[initiales]" placeholder="sh">
</label>
</li>

<li>
<label>
  Quel navigateur et quelle version de ce navigateur utilisez-vous pour remplir ce formulaire ?
  <input type="text" name="d[navigateur]" placeholder="Firefox 68.0.1" list="navigateurs">
  <datalist id="navigateurs">
    <option value="Firefox 68.0.1">
    <option value="Firefox ESR 60.8.0">
    <option value="Chromium 76.0.3809">
    <option value="Chrome 76.0.3809">
    <option value="Safari 12.1.2">
    <option value="Edge 44.18362.1.0">
    <option value="Opera 62.0.3331.99">
  </datalist>
</label>
</li>

<li>
<label>
  Avec le système d’exploitation
  <input type="text" name="d[os]" placeholder="GNU/Linux" list="os">
  <datalist id="os">
    <option value="GNU/Linux">
    <option value="Mac OS X">
    <option value="Windows">
    <option value="Android">
    <option value="iOS">
  </datalist>
</label>
</li>

<li>
<label>
  Est-ce que le remplissage et la soumission du formulaire fonctionne correctement ?<br>
  <input type="radio" name="d[remplissage]" value="oui">Oui<br>
  <input type="radio" name="d[remplissage]" value="non">Non<br>
  <input type="radio" name="d[remplissage]" value="nc" checked>Pas testé
</label>
</li>

<li>
<label>
  Est-ce que vous retrouvez le contenu correct de votre formulaire quand vous revenez plus tard (à l’url avec <tt>?key=</tt>) ?<br>
  <input type="radio" name="d[retour]" value="oui">Oui<br>
  <input type="radio" name="d[retour]" value="non">Non<br>
  <input type="radio" name="d[retour]" value="nc" checked>Pas testé
</label>
</li>

<li>
<label>
  Est-ce que vous pouvez modifier le contenu du formulaire quand vous revenez plus tard ?<br>
  <input type="radio" name="d[modification]" value="oui">Oui<br>
  <input type="radio" name="d[modification]" value="non">Non<br>
  <input type="radio" name="d[modification]" value="nc" checked>Pas testé
</label>
</li>

<li>
<label>
  Est-ce que l’export du contenu du formulaire en JSON fonctionne ?<br>
  <input type="radio" name="d[exportJSON]" value="oui">Oui<br>
  <input type="radio" name="d[exportJSON]" value="non">Non<br>
  <input type="radio" name="d[exportJSON]" value="nc" checked>Pas testé
</label>
</li>

<li>
<label>
  Est-ce que l’import du contenu du formulaire depuis JSON fonctionne ?<br>
  <input type="radio" name="d[importJSON]" value="oui">Oui<br>
  <input type="radio" name="d[importJSON]" value="non">Non<br>
  <input type="radio" name="d[importJSON]" value="nc" checked>Pas testé
</label>
</li>

<li>
<label>
  Est-ce que vous avez des remarques à faire, des détails à donner ?
  Vous pouvez utiliser du markdown et vérifier son rendu dans l’aperçu ci-dessous.<br>
  <textarea id="commentaires-mdwn" name="d[commentaires]" rows="15" cols="100" placeholder="Ce système est *parfait*."></textarea>
</label>
<details id="commentaires-apercu">
    <summary>Aperçu</summary>
    <div class="encart" id="commentaires-html"></div>
</details>
</li>

<li>
<label>
  Est-ce que le champ markdown ci-dessus fonctionne comme vous
  voudriez (rendu correct, pas trop gourmand en ressources, il se met
  effectivement à jour de temps en temps quand vous changez votre
  commentaire) ?<br>
  <input type="radio" name="d[champMarkdown]" value="oui">Oui<br>
  <input type="radio" name="d[champMarkdown]" value="non">Non<br>
  <input type="radio" name="d[champMarkdown]" value="nc" checked>Pas testé
</label>
</li>

<li>
<label>
  Votre <i>clef</i> (qui doit être générée automatiquement au chargement de la page normalement) :
  <input type="text" name="key" readonly required>
</label>
</li>

<li>
<label>
  Est-ce que la clef ci-dessus a été générée correctement ?<br>
  <input type="radio" name="d[clef]" value="oui">Oui<br>
  <input type="radio" name="d[clef]" value="non">Non<br>
  <input type="radio" name="d[clef]" value="nc" checked>Pas testé
</label>
</li>

</ul>

<input type="hidden" name="p[0]" value="_" readonly>

<p>Formulaire rempli <span id="nombre">0</span> fois.</p>

<button type="submit">Send</button>
<button type="button" onclick="callbacks.exportAsJSON();">Export as JSON</button>
<button type="button" onclick="callbacks.chooseFileForImport();">Import from JSON</button>
<input id="importButton" type="file" accept="application/json" style="opacity: 0;" onchange="callbacks.importFromJSON();">
</form>
<script>
  window.addEventListener('load', function() {
    callbacks = formin('formulaire', 'importButton', 24);
    <?php
      $key = isset($_REQUEST['key']) ? json_encode($_REQUEST['key']) : '';
      echo "callbacks.setKey({$key});\n";
      if(isset($GLOBALS['userData']['d']))
        echo "    callbacks.fillFromUserData('d', {$GLOBALS['userData']['d']});\n";
      if(isset($GLOBALS['userData']['p']))
        echo "    callbacks.fillFromUserData('p', {$GLOBALS['userData']['p']});\n";
      echo "    var publicData = {$GLOBALS['publicData']};\n";
    ?>
    cbMarked = slowMarkedRenderer('commentaires-mdwn', 'commentaires-html', false, 1000);
    var apercu = document.getElementById('commentaires-apercu');
    apercu.addEventListener('toggle', function() { cbMarked.toggle(apercu.open); });
    document.getElementById('nombre').innerHTML = Object.keys(publicData).length;
  });
</script>
</body>
</html>
