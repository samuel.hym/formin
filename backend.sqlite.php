<?php

/* Copyright 2019 Université de Lille
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.
 */

/* The following module must be included from a form after $config has
 * been set. If $config is not defined, we must abort.
 *
 * This module sets the following global variables:
 * -    $notifications,
 * -    $userData and $publicData when processing a GET
 */

if(!isset($config)) {
    header('Content-Type: text/html; charset=iso-8859-1', true, 404);
    $uri=htmlspecialchars($_SERVER['REQUEST_URI']);
    echo <<< MSG
<!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML 2.0//EN">
<html><head>
<title>404 Not Found</title>
</head><body>
<h1>Not Found</h1>
<p>The requested URL {$uri} was not found on this server.</p>
</body></html>

MSG;
    exit(1);
}

define("FORMIN_ERROR_INPUT_NOT_ENCODABLE", 1);
define("FORMIN_ERROR_JSON_ENCODING",       2);
define("FORMIN_ERROR_KEY_NOT_UTF8",        3);
define("FORMIN_ERROR_LARGE_TABLE",         4);
define("FORMIN_ERROR_LONG_INPUT",          5);
define("FORMIN_ERROR_LONG_KEY",            6);
define("FORMIN_ERROR_SHORT_KEY",           7);
define("FORMIN_ERROR_UNKNOWN_KEY",         8);

define("FORMIN_INTERNAL_ERROR",           30);

define("FORMIN_NOTIFICATION_SUCCESS_INSERT", 50);
define("FORMIN_NOTIFICATION_SUCCESS_UPDATE", 51);

define("FORMIN_ERROR_LOCATION_DB_CREATION",     1);
define("FORMIN_ERROR_LOCATION_GET1",            2);
define("FORMIN_ERROR_LOCATION_GET2",            3);
define("FORMIN_ERROR_LOCATION_INSERT",          4);
define("FORMIN_ERROR_LOCATION_INSERT_PREPARE",  5);
define("FORMIN_ERROR_LOCATION_OPEN",            6);
define("FORMIN_ERROR_LOCATION_TABLE_CREATION",  7);
define("FORMIN_ERROR_LOCATION_UPDATE",          8);
define("FORMIN_ERROR_LOCATION_UPDATE_EXEC",     9);
define("FORMIN_ERROR_LOCATION_IMPOSSIBLE",     10);

$GLOBALS['notifications'] = array("messages" => array());
$GLOBALS['userData'] = array();
$GLOBALS['publicData'] = '{}';

class ForminException extends RuntimeException {}

function notify($message, $details = NULL) {
    $fullmessage = array('message' => $message);
    if(isset($details))
        $fullmessage['details'] = $details;
    $GLOBALS['notifications']['messages'][] = $fullmessage;
}

function notifyAbort($status, $message, $details = NULL, $throwExn = true) {
    notify($message, $details);
    http_response_code($status);
    $GLOBALS['notifications']['status'] = $status;
    if($throwExn)
        throw new ForminException("{$message} - {$details}");
}

function setPublicData($db) {
    global $config;

    if($config['set_public_data']) {
        $result = $db->query("select rowid, p from `{$config['table']}` where p not null and p <> '';");
        $resp = array();
        while($data = $result->fetchArray())
            $resp[] = "\"{$data['rowid']}\": {$data['p']}";
        $result->finalize();
        $GLOBALS['publicData'] = "{" . join(",", $resp) . "}";
    }
}

function setUserData($db, $key) {
    global $config;

    $dbStmt = $db->prepare("select rowid, time, d, p from `{$config['table']}` where key = :key;");
    $dbStmt->bindValue(':key', $key, SQLITE3_TEXT);
    $result = $dbStmt->execute();
    if(!$result)
        notifyAbort(500, FORMIN_ERROR_UNKNOWN_KEY, FORMIN_ERROR_LOCATION_GET1);
    $resultRow = $result->fetchArray();
    if($resultRow) {
        $GLOBALS['userData']['id'] = $resultRow['rowid'];
        $GLOBALS['userData']['time'] = $resultRow['time'];
        if(isset($resultRow['d'])) $GLOBALS['userData']['d'] = $resultRow['d'];
        if(isset($resultRow['p'])) $GLOBALS['userData']['p'] = $resultRow['p'];
    } else
        notifyAbort(404, FORMIN_ERROR_UNKNOWN_KEY, FORMIN_ERROR_LOCATION_GET2);
    $dbStmt->close();
}

function setUserDataFromPost() {
    if(isset($_POST['d']))
        $GLOBALS['userData']['d'] = json_encode($_POST['d'], JSON_PARTIAL_OUTPUT_ON_ERROR);
    if(isset($_POST['p']))
        $GLOBALS['userData']['p'] = json_encode($_POST['p'], JSON_PARTIAL_OUTPUT_ON_ERROR);
}

function jsonEncode($raw) {
    $res = json_encode($raw, JSON_PARTIAL_OUTPUT_ON_ERROR);
    $err = json_last_error();
    if($err !== JSON_ERROR_NONE) {
        notifyAbort(400, FORMIN_ERROR_JSON_ENCODING, $err);
    }
    return $res;
}

function processPost($db) {
    global $config;

    $db->exec("create table if not exists `{$config['table']}` (key text not null, time integer not null, d text, p text, primary key(key));")
        || notifyAbort(500, FORMIN_INTERNAL_ERROR, FORMIN_ERROR_LOCATION_TABLE_CREATION);
    $keyLength = (isset($_POST['key'])) ? strlen($_POST['key']) : 0;
    if($keyLength < $config['key_min_length'])
        notifyAbort(401, FORMIN_ERROR_SHORT_KEY, $keyLength);
    if($keyLength > $config['key_max_length'])
        notifyAbort(413, FORMIN_ERROR_LONG_KEY);
    if(!mb_check_encoding($_POST['key'], "UTF8"))
        notifyAbort(401, FORMIN_ERROR_KEY_NOT_UTF8);
    $d = isset($_POST['d']) ? jsonEncode($_POST['d']) : NULL;
    $p = isset($_POST['p']) ? jsonEncode($_POST['p']) : NULL;
    if(strlen($p) > $config['p_max_length'] || strlen($d) > $config['d_max_length'])
        notifyAbort(413, FORMIN_ERROR_LONG_INPUT);
    $dbStmt = $db->prepare("update `{$config['table']}` set time = :time, d = :d, p = :p where key = :key;");
    if(!$dbStmt)
        notifyAbort(500, FORMIN_INTERNAL_ERROR, FORMIN_ERROR_LOCATION_UPDATE);
    $now = time();
    $dbStmt->bindValue(':key', $_POST['key'], SQLITE3_TEXT);
    $dbStmt->bindValue(':p', $p, SQLITE3_TEXT);
    $dbStmt->bindValue(':d', $d, SQLITE3_TEXT);
    $dbStmt->bindValue(':time', $now, SQLITE3_INTEGER);
    $result = $dbStmt->execute();
    if(!$result)
        notifyAbort(500, FORMIN_INTERNAL_ERROR, FORMIN_ERROR_LOCATION_UPDATE_EXEC);
    $changes = $db->changes();
    $result->finalize();
    $dbStmt->close();
    if($changes == 1)
        notify(FORMIN_NOTIFICATION_SUCCESS_UPDATE);
    elseif($changes == 0) {
        if(isset($config['table_max_length'])) {
            $curLength = $db->querySingle("select count(*) from `{$config['table']}`;");
            if($curLength >= $config['table_max_length'])
                notifyAbort(507, FORMIN_ERROR_LARGE_TABLE);
        }
        $dbStmt = $db->prepare("insert into `{$config['table']}` (key, time, d, p) values (:key, :time, :d, :p);");
        if(!$dbStmt)
            notifyAbort(500, FORMIN_INTERNAL_ERROR, FORMIN_ERROR_LOCATION_INSERT_PREPARE);
        $dbStmt->bindValue(':key', $_POST['key'], SQLITE3_TEXT);
        $dbStmt->bindValue(':time', $now, SQLITE3_INTEGER);
        $dbStmt->bindValue(':d', $d, SQLITE3_TEXT);
        $dbStmt->bindValue(':p', $p, SQLITE3_TEXT);
        $result = $dbStmt->execute();
        if($result) {
            $result->finalize();
            notify(FORMIN_NOTIFICATION_SUCCESS_INSERT);
        }
        else
            notifyAbort(500, FORMIN_INTERNAL_ERROR, FORMIN_ERROR_LOCATION_INSERT);
        $dbStmt->close();
    } else
        notifyAbort(500, FORMIN_INTERNAL_ERROR, FORMIN_ERROR_LOCATION_IMPOSSIBLE);
}

function processRequest() {
    global $config;

    switch($_SERVER['REQUEST_METHOD']) {
    case 'HEAD':
    case 'GET':
        /* We are just rendering the form, let us fetch the necessary data.
         * There is no need if we have no key and the form does contain
         * public data */
        if(isset($_GET['key']) || $config['set_public_data']) {
            try {
                $db = new SQLite3($config['database'], SQLITE3_OPEN_READONLY);
            }
            catch(Exception $e) {
                /* Let us hope that means the database was not created
                 * yet, so we just stop there if the user did not try
                 * explicitly to get a given key */
                if(isset($_GET['key']))
                    notifyAbort(404, FORMIN_ERROR_UNKNOWN_KEY, FORMIN_ERROR_LOCATION_OPEN, false);
                return;
            }

            try {
                setPublicData($db);
                if(isset($_GET['key']))
                    setUserData($db, $_GET['key']);
            }
            catch(ForminException $f) {}
            finally {
                $db->close();
            }
        }
        break;

    case 'POST':
        try {
            $db = new SQLite3($config['database']);
        }
        catch(Exception $e) {
            notifyAbort(500, FORMIN_INTERNAL_ERROR, FORMIN_ERROR_LOCATION_DB_CREATION, false);
            setUserDataFromPost();
            return;
        }
        try {
            processPost($db);
            setPublicData($db);
            setUserData($db, $_POST['key']);
        }
        catch(ForminException $f) {
            setUserDataFromPost();
        }
        $db->close();
        break;

    /* TODO? Allow DELETE to remove an entry */

    default:
        /* method not allowed */
        header('Allow: GET, POST, HEAD', true, 405);
        exit(1);
    }
}
