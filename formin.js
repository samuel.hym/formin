"use strict";

/* Copyright 2019 Université de Lille
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.
 */

function formin(mainForm, fileInput, keyLength) {

/* Utils */

function defined(v) {
    return !(typeof v === 'undefined');
}

function orElse(v, def) {
    return defined(v) ? v : def;
}

/* Does a pseudo-base64 encoding for UInt8Array */
function encode(data) { // data is a UInt8Array
    const b = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_";
    var res = [];
    for(var i = 0; i < data.length; i += 3) {
        const b1 = (orElse(data[i],0) >> 2) & 63;
        const b2 = ((orElse(data[i],0) & 3) << 4) | ((orElse(data[i+1],0) >> 4) & 15);
        const b3 = ((orElse(data[i+1],0) & 15) << 2) | ((orElse(data[i+2],0) >> 6) & 3);
        const b4 = orElse(data[i+2],0) & 63;
        res.push(b[b1] + b[b2] + b[b3] + b[b4]);
    }
    return res.join("");
}

/* Core management */

/* If the keyLength is not a multiple of 4, it will be rounded up */
function initialiseKey(form, keyLength, always) {
    if(always || form["key"].value == "") {
        let vals = new Uint8Array(Math.ceil(keyLength*3/4));
        window.crypto.getRandomValues(vals);
        let key = encode(vals);
        form["key"].value = key;
        return key;
    }
    return form["key"].value;
}

function toJSON(form) {
    function toObject(form) {
        var fd = new FormData(form);
        var obj = {};
        for (let [k,v] of fd){
            if (!(k in obj))
                obj[k] = v;
            else if (obj[k].constructor.name == 'String')
                obj[k] = [obj[k], v];
            else if (obj[k].constructor.name == 'Array')
                obj[k].push(v);
            /* When the value is empty, it might make sense to
             * get the placeholder instead, if the goal of the export
             * is to edit the file */
        }
        return obj;
    }

    return JSON.stringify(toObject(form));
}

function exportAsJSON(form, filename) {
    const prefix = 'data:application/json,';
    const data = encodeURIComponent(toJSON(form));
    var elem = document.createElement('a');
    elem.setAttribute('href', prefix + data);
    elem.setAttribute('download', orElse(filename, 'form-data.json'));
    document.body.appendChild(elem);
    elem.click();
    elem.remove();
}

function fillFromObject(form, obj) {
    function triggerInputEv(elem) {
        // trigger input event on elem
        if (elem.dispatchEvent) { // elem is single element, not a collection
            let inputEvent = document.createEvent('Event');  // old fashioned, IE compatible
            inputEvent.initEvent('input', true, true);
            elem.dispatchEvent(inputEvent);
        }
    }

    for(let k in obj) {
        if (form[k]) {
            switch (obj[k].constructor.name) { // String or Array (of Strings)
                case "String":
                    if (form[k].type === "checkbox")
                        form[k].checked = form[k].value === obj[k];
                    else
                        form[k].value = obj[k];
                    triggerInputEv(form[k]);
                    break;
                case "Array": // form field should be a select-multiple or a collection of checkboxes
                    let options, selectionName;
                    if (form[k].type === "select-multiple")
                        [options, selectionName] = [form[k].options, "selected"];
                    else if (typeof form[k][Symbol.iterator] === 'function' && form[k][0].type === 'checkbox')
                        // let's suppose it's a list of checkbox
                        [options, selectionName] = [form[k], "checked"];
                    for (let opt of options)
                        opt[selectionName] = obj[k].indexOf(opt.value) != -1;
                    triggerInputEv(form[k]);
                    break;
            }
        }
    }
}

function fillFromJSON(form, json) {
    /* We should maybe handle differently fields that are empty in the
     * JSON and not empty in the current form? */
    const obj = JSON.parse(json); /* we should probably handle parsing errors */
    fillFromObject(form,obj);
}

function fillFromUserData(form, dict, obj) {
    // rename keys:
    //  - add dict prefix
    //  - add [] postfix for array data
    let renamedObj = {};
    for (let key in obj) {
        let formkey = dict+'['+key+']';
        if (obj[key].constructor.name == "Array")
            formkey += "[]";
        renamedObj[formkey] = obj[key];
    }
    // fill form:
    fillFromObject(form,renamedObj);
}

function importFromJSON(form, file) {
    if(defined(file)) {
        var fr = new FileReader();
        fr.onload = function(e) {
            try {
                fillFromJSON(form, e.target.result);
            } catch(e) {
                console.log("Error while importing!", e.message);
            }
        };
        // TODO? Handle failure, if it happens for some reason
        fr.readAsText(file);
    } else {
        console.log('importFromJSON called with undefined');
    }
}

var mainFormEl = typeof(mainForm) === 'string' ? document.getElementById(mainForm) : mainForm;
var fileInputEl = typeof(fileInput) === 'string' ? document.getElementById(fileInput) : fileInput;

return {
    onSubmit:            function() { var key = initialiseKey(mainFormEl, keyLength);
                                      window.history.pushState({}, '', '?key=' + encodeURIComponent(key)); },
    exportAsJSON:        function(filename) {
                                      initialiseKey(mainFormEl, keyLength);
                                      exportAsJSON(mainFormEl, filename); },
    chooseFileForImport: function() { fileInputEl.value = null; fileInputEl.click(); },
    importFromJSON:      function() { importFromJSON(mainFormEl, fileInputEl.files[0]); },
    setKey:              function(key) {
                             if(defined(key)) {
                                 mainFormEl["key"].value = key;
                             } else {
                                 initialiseKey(mainFormEl, keyLength);
                             }
                         },
    fillFromUserData:    function(dict, data) { fillFromUserData(mainFormEl, dict, data); }
};

}
