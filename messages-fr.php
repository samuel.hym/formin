<?php

/* Copyright 2019 Université de Lille
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.
 */

if(!isset($config)) {
    header('Content-Type: text/html; charset=iso-8859-1', true, 404);
    $uri=htmlspecialchars($_SERVER['REQUEST_URI']);
    echo <<< MSG
<!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML 2.0//EN">
<html><head>
<title>404 Not Found</title>
</head><body>
<h1>Not Found</h1>
<p>The requested URL {$uri} was not found on this server.</p>
</body></html>

MSG;
    exit(1);
}

function showCriticalNotification($messageText) {
            echo <<< MSG
<div class="formin-notification formin-notification-critical">
  <b>Erreur fatale !</b><br>
  <p>{$messageText}</p>
</div>

MSG;
}

function showStored($how) {
    $base = htmlspecialchars($_SERVER['PHP_SELF']);
    $key = urlencode($_REQUEST['key']);
    echo <<< MSG
<div class="formin-notification formin-notification-normal">
  <b>Données enregistrées</b><br>
  <p>Vos données ont été correctement {$how}. Conservez ce <a href="{$base}?key={$key}">lien</a> pour éditer son contenu.</p>
</div>

MSG;
}

function showNotifications() {
    foreach($GLOBALS['notifications']['messages'] as $notification) {
        switch($notification['message']) {
        case FORMIN_ERROR_INPUT_NOT_ENCODABLE:
            showCriticalNotification("Le serveur ne peut pas accepter vos données : elles doivent être encodées en UTF-8.");
            break;
        case FORMIN_ERROR_JSON_ENCODING:
            showCriticalNotification("Le serveur ne peut pas encoder vos données en JSON : erreur {$notification['details']}.");
            break;
        case FORMIN_ERROR_KEY_NOT_UTF8:
            showCriticalNotification("Le serveur ne peut pas accepter votre clef : elle doit être encodée en UTF-8.");
            break;
        case FORMIN_ERROR_LARGE_TABLE:
            showCriticalNotification("Le serveur est saturé : contactez l’auteur du formulaire.");
            break;
        case FORMIN_ERROR_LONG_INPUT:
            showCriticalNotification("Vos données sont trop longues pour être acceptées.");
            break;
        case FORMIN_ERROR_LONG_KEY:
            showCriticalNotification("Votre clef est trop longue pour être acceptée : elle doit contenir au plus {$GLOBALS['config']['key_max_length']} octets.");
            break;
        case FORMIN_ERROR_SHORT_KEY:
            showCriticalNotification("Votre clef est trop courte pour être acceptée : elle doit contenir au moins {$GLOBALS['config']['key_min_length']} octets.");
            break;
        case FORMIN_ERROR_UNKNOWN_KEY:
            showCriticalNotification("Votre clef est inconnue : vos données ne sont pas sur le serveur.");
            break;
        case FORMIN_INTERNAL_ERROR:
            showCriticalNotification("Erreur interne numéro {$notification['details']}.");
            break;

        case FORMIN_NOTIFICATION_SUCCESS_INSERT:
            showStored("enregistrées");
            break;
        case FORMIN_NOTIFICATION_SUCCESS_UPDATE:
            showStored("mises à jour");
            break;
        }
    }
}
